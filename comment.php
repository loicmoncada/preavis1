<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/themes/light.css" />
<script type="module" src="https://cdn.jsdelivr.net/npm/@shoelace-style/shoelace@2.4.0/dist/shoelace-autoloader.js"></script>
<link rel="stylesheet" href="style.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<div>
    <?php
        include 'db/crud.php';
        include 'function.php';
        $avis = readAll('avis');
        $card =readAll('card');
        $id=$_GET['id'];
    ?>
    <div class='recap'>
        <h2><?php print_r($card[$id-1]['nom'])?></h2>
        <img src="<?php print_r($card[$id-1]['photo'])?>" alt="">  
        <sl-rating label="Rating" readonly value="<?= moyenne($card[$id-1]['id'])[0][0] ?>"></sl-rating>
    </div>
    <div>
        <form id='comment' action="print.php" method="post">
            <textarea name="text" id="" cols="30" rows="5"></textarea>

            <sl-rating id='ratting' label="Rating" precision="0.1" value="0"></sl-rating>
            <input type="hidden" name="idH" value="<?= $_GET['id']?>">
            <script src='recupSl.js'></script>
            <input class="btn btn-primary" type="submit" value="OK">
        </form>
    </div>
    <div>
    <?php 
        
        
        
        $com =linkerA($id);
        foreach($com as $a){
            echo comment($id,$a['text'],$a['note']);
        }
        
        
        ?>
    </div>
</div>