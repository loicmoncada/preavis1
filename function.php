<?php

function card($id,$title,$photo,$moyenne){
    echo '<sl-card num='.$id.' class="card-overview">
        <img slot="image" src="'.$photo.'"/>
        <strong>'.$title.'</strong><br />
        <div slot="footer">
          <a href="comment.php?id='.$id.'"> <sl-button variant="primary" pill>More Info</sl-button></a>
          <sl-rating label="Rating" readonly value="'.$moyenne.'"></sl-rating>
        </div>
      </sl-card>
      ';
}
function comment($id,$text,$note){
    echo '<sl-card class="card-header" num='.$id.'>
            <sl-rating label="Rating" readonly value="'.$note.'"></sl-rating>
        
            '.$text.'
          </sl-card>
      ';
}
?>


