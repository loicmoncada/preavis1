drop database if exists avis; 
create database avis;
use avis;
create table card(
    id int not null auto_increment primary key,
    nom varchar(255) not null,
    photo varchar(255) not null
);
create table avis(
    id int not null auto_increment primary key,
    text varchar(255) ,
    note float 
);
create table link(
    id_card int,
    id_avis int
);

drop USER if exists 'admin'@'127.0.0.1';
CREATE USER 'admin'@'127.0.0.1' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON avis.* TO 'admin'@'127.0.0.1';